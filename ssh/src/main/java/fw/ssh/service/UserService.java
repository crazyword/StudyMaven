package fw.ssh.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fw.ssh.dao.UserDao;
import fw.ssh.entity.User;

@Service
public class UserService {
	
	@Autowired
	private UserDao userDao;
	
	public void add(String name, String password){
		User user = new User();
		user.setName(name);
		user.setPassword(password);
		userDao.getHibernateTemplate().save(user);
	}
	
	public Optional<User> find(User user){
		List<User> userList = userDao.getHibernateTemplate().findByExample(user);
		if(userList.size()>0){
			return Optional.of(userList.get(0));
		}else{
			return Optional.empty();
		}
	}

	public void setGenericDao(UserDao userDao) {
		this.userDao = userDao;
	}

	
}
